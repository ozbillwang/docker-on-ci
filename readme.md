# Docker on CI

- Github Action - [![Github Action](https://github.com/ozbillwang/docker-on-ci/workflows/build/badge.svg)](https://github.com/ozbillwang/docker-on-ci/actions)

- Gitlab CI - [![GitlabCI](https://gitlab.com/jeremie.drouet/docker-on-ci/badges/master/pipeline.svg)](https://gitlab.com/jeremie.drouet/docker-on-ci/pipelines)

- Circle CI - [![CircleCI](https://circleci.com/gh/ozbillwang/docker-on-ci.svg?style=svg)](https://circleci.com/gh/ozbillwang/docker-on-ci)

- Travis CI - [![Build Status](https://app.travis-ci.com/ozbillwang/docker-on-ci.svg?branch=master)](https://app.travis-ci.com/ozbillwang/docker-on-ci)

### Ref with fixes

https://www.docker.com/blog/multi-arch-build-what-about-travis/

https://www.docker.com/blog/multi-arch-build-what-about-circleci/

Due to these free online pipelines keep changing with docker itself keep enhancing, **these builds are triggered per week** to confirm the stabilities. 
